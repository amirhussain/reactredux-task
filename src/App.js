import React from 'react';
import './App.css';
import Add from './components/add';

function App() {

  let styles = {
    parentDiv: {
      width: '100%'
    },
    childDiv1: {
      width: '50%'
    }
  }
  return (
    <div className="App">
      <div style={styles.parentDiv}>
        <div style={styles.childDiv1}>
          <Add />
        </div>
      </div>
    </div>
  );
}

export default App;
