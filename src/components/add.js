import React, { useState } from 'react';

const Add = () => {

    const [firstNum, setFisrtNum] = useState('');
    const [secondNum, setSecondNum] = useState('');
    let [sum, setSum] = useState(0);

    const sumOfNumbers = (evt) => {
        if (firstNum !== undefined && secondNum !== undefined) {
            sum = parseInt(firstNum) + parseInt(secondNum);
            setSum(sum);
        }
    }

    return (
        <div>
            <div>
                <input
                    type="number"
                    value={firstNum}
                    placeholder="Enter first number"
                    onChange={e => setFisrtNum(e.target.value)}
                />
            </div>
            <div>
                <input
                    type="number"
                    value={secondNum}
                    placeholder="Enter second number"
                    onChange={e => setSecondNum(e.target.value)}
                />
            </div>
            <div>
                <input
                    type="submit"
                    value="Submit"
                    onClick={sumOfNumbers}
                />
            </div>
            <div>
                The sum of the above two numbers is {sum}
            </div>
        </div>

    )
}

export default Add